# Element (former Riot) Desktop AppImage

[element-desktop](https://github.com/vector-im/element-desktop) is a universal secure chat app entirely under your control from [element-io](https://element.io/)

Download **Element.AppImage** binary from here: [Element.AppImage](https://gitlab.com/linuxappimage/element-desktop/-/jobs/artifacts/master/raw/Element.AppImage?job=run-build)

Current version:

VERSION=v1.11.95

Notes for you Linux shell:

```bash
curl -sLo Element.AppImage https://gitlab.com/linuxappimage/element-desktop/-/jobs/artifacts/master/raw/Element.AppImage?job=run-build

chmod +x Element.AppImage
./Element.AppImage

```

## Disclaimer

This repo contains just a CI/CD pipeline to build the AppImage from source code. As you can see from this repo, there is no code. Just a patch to package.json so we can build the AppImage format from element-desktop.

